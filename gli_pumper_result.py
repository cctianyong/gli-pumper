#!/usr/bin/python
import sys
import re
import os

if( len( sys.argv ) < 3 ) :
  print "Usage: gli_pumper_result.py  <sender configuration file> <rcvr configuration file> "
  exit( -1 )

sender_cfg = sys.argv[1]
rcvr_cfg = sys.argv[2]

print "Sender configuration file: ", sender_cfg
print "Rcvr   configuration file: ", rcvr_cfg

sender_log = ""
rcvr_log = ""

traffic_type = ""

sender_cfg_file = file( sender_cfg,  'r' )
for line in sender_cfg_file:
  line_str = line.strip( '\n' )
  list = re.split( '\s*=\s*', line_str )
  key = list[0]
  val = list[1]
  if( cmp( key, "LOGFILE" ) == 0 ):
    sender_log = val
  if( cmp( key, "TYPE" ) == 0 ):
    traffic_type = val

rcvr_cfg_file = file( rcvr_cfg,  'r' )
for line in rcvr_cfg_file:
  line_str = line.strip( '\n' )
  list = re.split( '\s*=\s*', line_str )
  key = list[0]
  val = list[1]
  if( cmp( key, "LOGFILE" ) == 0 ):
    rcvr_log = val

print "Sender Log:  ", sender_log
print "Rcvr   Log:  ", rcvr_log

cmd_res = os.popen( "tail -1 %s" % sender_log ).readlines()
sender_line = cmd_res[0]
cmd_res = os.popen( "tail -1 %s" % rcvr_log ).readlines()
rcvr_line = cmd_res[0]

print "Test Summary:"
print "    Traffic Type            : ", traffic_type

if( cmp( traffic_type, "SYN-FLOOD" ) == 0 ):
  #print "sender line ", sender_line
  #print "rcvr line ", rcvr_line
  m = re.findall( r"\[SYN_SENT=(.+?)\]", sender_line );
  syn_sent_count = m[0]
  m = re.findall( r"\[SYN_RCVD=(.+?)\]", rcvr_line );
  syn_rcvd_count = m[0]
  print "    Flooding SYN packets count : ", syn_sent_count
  print "    Arrives target count       : ", syn_rcvd_count
  pass_rate = float( syn_rcvd_count ) / float( syn_sent_count ) * 100
  defend_rate = 100 - pass_rate
  print "    SYN flood traffic pass rate: ", round( pass_rate, 2 ), "%"
  print "    Flood protection rate      : ", round( defend_rate, 2), "%"
  
if( cmp( traffic_type, "HTTP-FLOOD" ) == 0 ):
  m = re.findall( r"\[SYN_SENT=(.+?)\]", sender_line );
  syn_sent_count = m[0]
  print "    Tcp connection attempts : ", syn_sent_count
  m = re.findall( r"\[SYNACK_RCVD=(.+?)\]", sender_line );
  synack_rcvd_count = m[0]
  print "    Established count       : ", synack_rcvd_count
  success_rate = float( synack_rcvd_count ) / float( syn_sent_count ) * 100
  print "    Success rate            : ", round( success_rate, 2 ), "%"

  m = re.findall( r"\[HTTP_REQ_SENT=(.+?)\]", sender_line );
  http_req_count = m[0]
  print "    Http request count      : ", http_req_count
  m = re.findall( r"\[HTTP_RSP_RCVD=(.+?)\]", sender_line );
  http_rsp_count = m[0]
  print "    Http response count     : ", http_rsp_count
  success_rate = float( http_rsp_count ) / float( http_req_count ) * 100
  print "    Complete rate           : ", round( success_rate, 2 ), "%"
