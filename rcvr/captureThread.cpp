#include <iostream>
#include <map>
#include <list>
#include "captureThread.h"
#include "sniffer.h"
#include "tcpUtils.h"
#include "pumper.h"
#include "global.h"

extern struct pumper_cfg g_cfg;
using namespace std;
//packets to send
extern list<Pkt> pktsToSend;
//SYN sent conns
extern map<string, ConnInfo> connsInited;
//shaked conns
extern map<string, ConnInfo> connsEstab;
//lock for SYN sent conns
extern pthread_mutex_t connMutex;
//lock for pktsToSend list operation
extern pthread_mutex_t ackMutex;  
//lock for counters
extern pthread_mutex_t countMutex;

extern bool v; 	//verbose

void processPacket(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet);

void* capEngineThread(void *arg)
{
	if(v)cout << "Capture Engine started " << endl;
	Sniffer *snf = (Sniffer *)arg;
	snf->readPackets(processPacket, (u_char *)arg);
	if(v)cout << "Capture Engine stopped " << endl;
	return 0;
}

void processPacket(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet)
{
	Sniffer *snf = (Sniffer *)args;
	map<string, ConnInfo>::iterator it;

	u_long dstIp = snf->getDstIpRaw(packet);
	int dstPort = snf->getTcpDstPort(packet);

  if( dstPort != g_cfg.dstport ){
    return;
  }
	
  if(snf->isSyn(packet)){
    pthread_mutex_lock(&countMutex);
    (*snf->synNo)++;			
    pthread_mutex_unlock(&countMutex);

		//This is the ACK packet that we send
    if( g_cfg.type == SYN_FLOOD_NO_ACK ){
      return;
    }
		Pkt pkt; 
		pkt.srcIp = snf->getDstIpRaw(packet);
		pkt.dstIp = snf->getSrcIpRaw(packet);
		pkt.srcPort = snf->getTcpDstPort(packet);
		pkt.dstPort = snf->getTcpSrcPort(packet);
		pkt.seqn = snf->getTcpACK(packet);
		pkt.ackn = snf->getTcpSEQ(packet) + 1;
    pkt.isSYN = true;
    pkt.isACK = true;
    pkt.isRST = false;

		pthread_mutex_lock(&ackMutex);
		pktsToSend.push_back(pkt);			
		pthread_mutex_unlock(&ackMutex);
  }else { //DATA
    if( g_cfg.type != HTTP_FLOOD ){
      return;
    }

    pthread_mutex_lock(&countMutex);
    (*snf->httpReqNo)++;			// increase number of finished connections
    pthread_mutex_unlock(&countMutex);

		//This is the ACK packet that we send
		Pkt pkt; 
		pkt.srcIp = snf->getDstIpRaw(packet);
		pkt.dstIp = snf->getSrcIpRaw(packet);
		pkt.srcPort = snf->getTcpDstPort(packet);
		pkt.dstPort = snf->getTcpSrcPort(packet);
		pkt.seqn = snf->getTcpACK(packet);
		pkt.ackn = snf->getTcpSEQ(packet) + 1;
    pkt.isSYN = false;
    pkt.isACK = true;
    pkt.isFIN = true;

		pthread_mutex_lock(&ackMutex);
		pktsToSend.push_back(pkt);			
		pthread_mutex_unlock(&ackMutex);
	} 
}

