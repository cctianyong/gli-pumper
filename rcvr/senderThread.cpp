#include "senderThread.h"
#include "tcpUtils.h"
#include "global.h"
#include "sniffer.h"
#include <iostream>
#include <list>
extern pumper_cfg g_cfg;

u_char *payload1 = (u_char*)
"GET / HTTP/1.1\r\n\
Host: hostname\r\n\
User-Agent: Gli-Pumper\r\n\r\n";


u_char *payload2 = (u_char*)
"~~~!!This is a INVALID HTTP!!~~~";

u_char *payload3 = (u_char*)
"200 OK";


list<Pkt> pktsToSend;
map<string, ConnInfo> connsInited;
map<string, ConnInfo> connsEstab;    
pthread_mutex_t connMutex = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t countMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ackMutex = PTHREAD_MUTEX_INITIALIZER; 

//void sendAck( u_char *packet, int packet_size, int network, 
//                            int v, string victimIpStr, int requestType );
void sendAck( u_char *packet, int packet_size, struct libnet_link_int * network, int v, string victimIpStr, int requestType );
void sendRequest( Pkt pkt, int network, int type, int v );
int getNumConnsEstab();


// This thread sends SYN packets to initiate TCP connsInited
// and store the information about them into connsInited map
void* senderThread(void *arg)
{
    SenderThreadArg *targ = (SenderThreadArg*)arg;

    string error;
    u_long localIp = 0, sourceIp = 0;
    string localIpStr;

    string ifName     = targ->ifName;
    bool doSpoof     = targ->doSpoof;
    unsigned long long count         = targ->count;
    int wait         = targ->wait;
    int v            = targ->verbose;
    u_long victimIp    = targ->victimIp;
    string victimIpStr = (char*)libnet_host_lookup(victimIp, 0);
    int victimPort    = targ->victimPort;
    int requestType = targ->requestType;
    int threadId     = targ->threadId;
    string srcNetStr= targ->srcNetStr;

    if(v)cout << "Sender thread " << threadId << " started" << endl;

    targ->threadId++;

    // Initialize packet sending component
    //int network = libnet_open_raw_sock(IPPROTO_RAW);
    char error_buf[255];
    bzero( error_buf, 255 );
    struct libnet_link_int *network = libnet_open_link_interface( g_cfg.interface, error_buf ) ;
    if (network == NULL)
    {
        libnet_error(LIBNET_ERR_FATAL, (char*)"Can't open interface. Are you root? Sender thread exit\n");
        pthread_exit(NULL);
    }

    // Get local IP address
    if(!(localIp = getLocalIp(ifName, error))) {
        cout << "Error while getting local IP address: " << error << endl;
        pthread_exit(NULL);
    }
    localIpStr = (char*)libnet_host_lookup(localIp, 0);

    // Initialize the generic packet
    //int packet_size = LIBNET_IP_H + LIBNET_TCP_H; // no payload
    int packet_size = LIBNET_ETH_H + LIBNET_IP_H + LIBNET_TCP_H; // no payload
    u_char *packet;
    libnet_init_packet(packet_size, &packet);
    if (packet == NULL) {
        libnet_error(LIBNET_ERR_FATAL, (char*)"libnet_init_packet failed\n");
        pthread_exit(NULL);
    }

    libnet_seed_prand();
    //sourceIp = libnet_name_resolve((u_char *)"192.168.10.55", LIBNET_RESOLVE);
    sourceIp = localIp;

    //don't start to send until we receive the signal
    while(*(targ->run) == false) usleep(100);    

    // send SYN packets
    while(*(targ->run)) 
    {
        // send one ACK packet from queue
        pthread_mutex_lock(&ackMutex);
        if(!pktsToSend.empty()) {
            //libnet_init_packet(packet_size, &packet);
            //if (packet == NULL) {
            //    libnet_error(LIBNET_ERR_FATAL, (char*)"libnet_init_packet failed\n");
             //   pthread_exit(NULL);
            //}
            sendAck(packet, packet_size, network, v, victimIpStr, requestType);
            pthread_mutex_unlock(&ackMutex);

            //pthread_mutex_lock(&countMutex);
            //(*targ->connNo)++;
            //pthread_mutex_unlock(&countMutex);
        } else {
            pthread_mutex_unlock(&ackMutex);
            usleep( 200 );
        }
    }
    //Cleanup
//    if (libnet_close_raw_sock(network) == -1)
    if (libnet_close_link_interface(network) == -1)
    {
        libnet_error(LN_ERR_WARNING, (char*)"libnet_close_raw_sock couldn't close the interface");
    }
    libnet_destroy_packet(&packet);

    if(v)cout << "Sender thread " << threadId << " finished" << endl;

    pthread_exit(NULL);;
}


// This function sends one ACK to complete the 3-way handshake
//void sendAck(u_char *packet, int packet_size, int network, 
//            int v, string victimIpStr, int requestType)
void sendAck(u_char *packet, int packet_size, struct libnet_link_int * network, 
            int v, string victimIpStr, int requestType)
{
    Pkt pkt = pktsToSend.front();
    pktsToSend.pop_front();

    if( pkt.isSYN ){
      pthread_mutex_lock(&countMutex);
      g_cfg.synackNo++;
      pthread_mutex_unlock(&countMutex);
      TcpUtils::buildSYNACK(packet, 
                pkt.srcIp, pkt.dstIp, 
                pkt.srcPort, pkt.dstPort, 
                pkt.seqn, pkt.ackn);
    }else if( pkt.isRST ){
      pthread_mutex_lock(&countMutex);
      g_cfg.rstNo++;
      pthread_mutex_unlock(&countMutex);
      TcpUtils::buildRSTACK(packet, 
                pkt.srcIp, pkt.dstIp, 
                pkt.srcPort, pkt.dstPort, 
                pkt.seqn, pkt.ackn);
    }else if( pkt.isFIN ){
      pthread_mutex_lock(&countMutex);
      g_cfg.finNo++;
      g_cfg.httpRspNo++;
      pthread_mutex_unlock(&countMutex);
      //TcpUtils::buildACK(packet, 
      //          pkt.srcIp, pkt.dstIp, 
      //          pkt.srcPort, pkt.dstPort, 
      //          pkt.seqn, pkt.ackn);

      u_char *dataPacket;
      int dataPackSize = LIBNET_ETH_H + LIBNET_IP_H + LIBNET_TCP_H + strlen((char *)payload3 );
      libnet_init_packet(dataPackSize, &dataPacket);
      if( dataPacket != NULL ){
        TcpUtils::buildTcpData( dataPacket, 
            pkt.srcIp, pkt.dstIp,
            pkt.srcPort, pkt.dstPort,
            pkt.seqn, pkt.ackn,
            payload3 );
        if( libnet_write_link_layer( network, g_cfg.interface, dataPacket, dataPackSize) < dataPackSize ){
            libnet_error(LN_ERR_WARNING, (char*)"libnet_write_ip only wrote less then %d bytes \n", dataPackSize );
        }
        libnet_destroy_packet(&dataPacket);
      }
    }

    int sent_size = 0;
    
    //sent_size = libnet_write_ip(network, packet, packet_size);
    //sent_size = packet_size;
    sent_size = libnet_write_link_layer(network, g_cfg.interface, packet, packet_size);
    //printf( "packet size %d sent size %d src IP %x dst IP %x, src port %d dst port %d, seq %d, ack %d\n", packet_size, sent_size, pkt.srcIp, pkt.dstIp, pkt.srcPort, pkt.dstPort, pkt.seqn, pkt.ackn );
    if( sent_size < packet_size ){
      libnet_error(LN_ERR_WARNING, (char*)"libnet_write_ip only wrote less then %d bytes\n", packet_size);
      usleep( 1000 );
    }


/*
    // Add new entry to connsEstab map
    ConnInfo connInfo;
    connInfo.srcIp = pkt.srcIp;
    connInfo.srcPort = pkt.srcPort;
    connInfo.state = STATE_ESTABLISHED;
    connsEstab[connId(pkt.srcIp, pkt.srcPort)] = connInfo; 
    if(requestType != 0) {
        sendRequest(pkt, network, requestType, v);
    }
*/
}


void sendRequest(Pkt pkt, int network, int type, int v) 
{
    u_char* payload = NULL;

    switch(type) {
        case HTTP_VALID:
            payload = payload1;
            break;
        case HTTP_INVALID:
            payload = payload2;
            break;
        default:
            if(v)cout << "Unknown request type" << endl;
            return;
    }

    u_char *dataPacket;
    int dataPackSize = LIBNET_IP_H + LIBNET_TCP_H + strlen((char *)payload);
    libnet_init_packet(dataPackSize, &dataPacket);
    if (dataPacket != NULL) {
        TcpUtils::buildTcpData(dataPacket, 
                    pkt.srcIp, pkt.dstIp,
                    pkt.srcPort, pkt.dstPort, 
                    pkt.seqn, pkt.ackn, 
                    payload);
        if (libnet_write_ip(network, dataPacket, dataPackSize) < dataPackSize) {
              libnet_error(LN_ERR_WARNING, (char*)"libnet_write_ip only wrote less then %d bytes\n", dataPackSize);
        }
        if(v)cout << "HTTP request packet sent" << endl;
        libnet_destroy_packet(&dataPacket);
    } else {
        if(v)cout << "libnet_init_packet failed" << endl;
    }
}

int getNumConnsEstab()
{
    int num;
    pthread_mutex_lock(&ackMutex);
    num = connsEstab.size();
    pthread_mutex_unlock(&ackMutex);

    return num;
}





