#include <iostream>
#include <map>
#include <list>
#include "captureThread.h"
#include "sniffer.h"
#include "tcpUtils.h"
#include "pumper.h"
#include "global.h"

using namespace std;
//packets to send
extern list<Pkt> pktsToSend;
//SYN sent conns
//extern map<string, ConnInfo> connsInited;
extern map<string, string> connsInited;
//shaked conns
//extern map<string, ConnInfo> connsEstab;
extern map<string, string> connsEstab;
//lock for SYN sent conns
extern pthread_mutex_t connMutex;
//lock for pktsToSend list operation
extern pthread_mutex_t ackMutex;  
//lock for counters
extern pthread_mutex_t countMutex;

extern bool v; 	//verbose
extern pumper_cfg g_cfg;

void processPacket(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet);

void* capEngineThread(void *arg)
{
	if(v)cout << "Capture Engine started " << endl;
	Sniffer *snf = (Sniffer *)arg;
	snf->readPackets(processPacket, (u_char *)arg);
	if(v)cout << "Capture Engine stopped " << endl;
	return 0;
}

void processPacket(u_char *args, const struct pcap_pkthdr* pkthdr, const u_char* packet)
{
	Sniffer *snf = (Sniffer *)args;
//	map<string, ConnInfo>::iterator it;
	map<string, string>::iterator it;

	u_long dstIp = snf->getDstIpRaw(packet);
	int dstPort = snf->getTcpDstPort(packet);
	
	if(snf->isRst(packet)) {
		if(v)
			cout << "Got RST-ACK packet from " 
				<< snf->getSrcIp(packet) 
				<< " to " << snf->getDstIp(packet) 
				<< endl;
	} else if(snf->isFin(packet)){
		if(v)
			cout << "Got FIN-ACK packet from " 
				<< snf->getSrcIp(packet) 
				<< " to " 
				<< snf->getDstIp(packet) 
				<< endl;

		pthread_mutex_lock(&connMutex);

		it = connsEstab.find(connId(dstIp, dstPort));

		// we received FIN for an established TCP connection
		if(it != connsEstab.end()) { 	
			connsEstab.erase(it);		// delete the established connections
			pthread_mutex_unlock(&connMutex);

			pthread_mutex_lock(&countMutex);
      g_cfg.finNo++;
      g_cfg.httpRspNo++;
			//(*snf->finNo)++;			// increase number of finished connections
			pthread_mutex_unlock(&countMutex);

		} else {
			pthread_mutex_unlock(&connMutex);
		}

	} else if(snf->isSyn(packet)){
		if(v)
			cout << "Got SYN-ACK packet from " << snf->getSrcIp(packet) << " to " << snf->getDstIp(packet) << endl;
	}

	pthread_mutex_lock(&connMutex);

	it = connsInited.find(connId(dstIp, dstPort));
	//SYN-ACK is back
	if(it != connsInited.end()) {
		if(v)
			cout << "Found session " << endl;

		connsInited.erase(it);
		pthread_mutex_unlock(&connMutex);

		if(snf->isRst(packet)) {
			pthread_mutex_lock(&countMutex);
			(*snf->rstNo)++;
			pthread_mutex_unlock(&countMutex);
			return;

		} else if(snf->isFin(packet)) {
			pthread_mutex_lock(&countMutex);
			(*snf->finNo)++;
			pthread_mutex_unlock(&countMutex);
			return;
		} else if(snf->isSyn(packet)) {
			pthread_mutex_lock(&countMutex);
			(*snf->synackNo)++;
			pthread_mutex_unlock(&countMutex);
		} 

		//This is the ACK packet that we send
		Pkt pkt; 
		pkt.srcIp = snf->getDstIpRaw(packet);
		pkt.dstIp = snf->getSrcIpRaw(packet);
		pkt.srcPort = snf->getTcpDstPort(packet);
		pkt.dstPort = snf->getTcpSrcPort(packet);
		pkt.seqn = snf->getTcpACK(packet);
		pkt.ackn = snf->getTcpSEQ(packet) + 1;

		pthread_mutex_lock(&ackMutex);

		//ACKs to sent for 3-way handshake
		pktsToSend.push_back(pkt);			

		pthread_mutex_unlock(&ackMutex);
	} else {
		pthread_mutex_unlock(&connMutex);
	}
}

