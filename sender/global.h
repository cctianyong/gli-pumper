#ifndef _GLOBAL_H
#define _GLOBAL_H
enum role { ROLE_SENDER = 1, ROLE_RCVR };
enum traffic { SYN_FLOOD = 1, HTTP_FLOOD };
enum http_traffic {  VALID_REQ = 1, INVALID_REQ };
struct pumper_cfg {
	int			role;
	int			speed; 			//conns per second	
	int			type;
	char		srcaddr[64];		//random or IP
	char		dstaddr[64];		//IP
	int			dstport;		//Port					
	int			http_type;
	int			thread_num; //Thread number
	char		interface[64];			
	unsigned long long 	msg_count; 
	int			duration;		//Seconds
	char		logfile[128];
  unsigned int interval;
  unsigned int burst;


  unsigned long long  synNo;
  unsigned long long  synackNo;

  unsigned long long  httpReqNo;
  unsigned long long  httpRspNo;

  unsigned long long  finNo;
  unsigned long long  rstNo;
  unsigned long long  connNo;
};


#endif
