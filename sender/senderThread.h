#ifndef __SENDER_THREAD_H__
#define __SENDER_THREAD_H__

#include "pumper.h"
#include <string>

using namespace std;
#define MAX_THREAD_NUM 4
class SenderThreadArg {
    public:
        string ifName;
        bool doSpoof;
        unsigned long long count;
        unsigned long long *synNo;     //connections initiated
        unsigned long long *connNo; //connections completed
        int wait;
        int verbose;
        u_long victimIp;
        int victimPort;
        string srcNetStr;
        int threadId;
        int requestType;
        bool *run;
        int thread_quota[MAX_THREAD_NUM];
        pthread_mutex_t thread_locks[MAX_THREAD_NUM];
};

void *senderThread(void *arg);
int getNumConnsEstab();

#endif

